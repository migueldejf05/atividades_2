//Operador OU 
namespace operador_OU

{
    let idade = 16; 
    let maiorIdade = idade > 18; 
    let possuiAutorizacaoDosPais = true; 

    let podeBeber = maiorIdade || possuiAutorizacaoDosPais; 

    console.log(podeBeber); // true
}