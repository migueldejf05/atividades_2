/*2-) A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente a um trabalho de laboratório, a uma avaliação semestral e a um exame final. A média das três notas mencionadas anteriormente obedece aos pesos a seguir.*/

namespace exercicio_2

{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;

    nota1 = 10;
    nota2 = 8.5;
    nota3 = 8;
    peso1 = 2;
    peso2 = 3;
    peso3 = 5;

    let media: number

    //processo

    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3)

    console.log (`a media do aluno é: ${media}`)

    //condições 

    if (media >= 8 && media <= 10)
{
    console.log ("Sua nota é A")

}
    if (media >=7 && media < 8)
{
    console.log ("Sua nota é B")
}
    if (media >= 6 && media < 7)
{
    console.log ("Sua nota é C")
}
    if (media >=5 && media < 6)
{
    console.log ("Sua nota é D")
}
    if (media >= 0 && media < 5)
{
    console.log ("Sua nota é E")
}
//fim
}