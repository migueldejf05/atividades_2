/*5-) Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada de acordo com o horário do dia (bom dia, boa tarde ou boa noite).*/

namespace exercicio_4

{
    const nome = "Miguel";
    const data = new Date ();
    const horas = data.getHours();

    if (horas >= 6 && horas < 12)
{
    console.log (`bom dia ${nome}`)
}
    else if (horas >= 12 && horas < 18)
{
    console.log (`boa tarde ${nome}`)
}
    else if (horas >= 18 && horas < 5)
{
    console.log (`boa noite ${nome}`)
}
}